package com.example.alexis.examenib

import android.content.Context
import android.net.Uri
import android.os.Bundle
import android.os.CountDownTimer
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.core.view.isVisible
import org.w3c.dom.Text
import kotlin.random.Random.Default.nextInt


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Activities that contain this fragment must implement the
 * [Addition.OnFragmentInteractionListener] interface
 * to handle interaction events.
 * Use the [Addition.newInstance] factory method to
 * create an instance of this fragment.
 *
 */
class Addition : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null
    private var listener: OnFragmentInteractionListener? = null
    private var score: Int = 0
    private var tiempo: Int = 0
    private var numeroRondas: Int = 1
    private var primerDigito: Int = 0
    private var segundoDigito: Int = 0
    //private var initialCountDown: Long = 10000
    private var countDownInterval: Long = 1000
    private lateinit var countDownTimer: CountDownTimer //implementa el Timer.
    private var timeLeft: Int = 10
    private lateinit var txt_score: TextView
    private lateinit var txt_time_left: TextView
    private lateinit var txt_primer_digito: TextView
    private lateinit var txt_segundo_digito: TextView
    private lateinit var txt_round: TextView
    private lateinit var botonRespuesta: Button
    private lateinit var operador: TextView

    private lateinit var inputRespuesta: EditText

    private var tipoOperador: String = ""


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view:View = inflater.inflate(R.layout.fragment_addition, container, false)
        tipoOperador = arguments?.getString("tipoOperador") ?: "+"
        operador = view.findViewById(R.id.operador) as TextView
        txt_round = view.findViewById(R.id.round) as TextView
        txt_score = view.findViewById(R.id.score) as TextView
        txt_time_left = view.findViewById(R.id.timeLeft) as TextView
        txt_primer_digito = view.findViewById(R.id.primerDigito) as TextView
        txt_segundo_digito = view.findViewById(R.id.segundoDigito) as TextView
        botonRespuesta = view.findViewById(R.id.enviar) as Button
        inputRespuesta = view.findViewById(R.id.inputText) as EditText
        txt_round.text = getString(R.string.round, numeroRondas)
        txt_score.text = getString(R.string.score, 0)
        txt_time_left.text = getString(R.string.time_left, timeLeft)
        primerDigito = nextInt(1,20)
        segundoDigito = nextInt(1,primerDigito)
        txt_primer_digito.text = primerDigito.toString()
        txt_segundo_digito.text = segundoDigito.toString()

        comprobarOperacion(tipoOperador)

        botonRespuesta.setOnClickListener { comprobarRespuesta() }

        iniciarCronometro()
        countDownTimer.start()
        return view

    }

    fun comprobarRespuesta(){
        var respuesta = inputRespuesta.text.toString().toDouble()
        countDownTimer.cancel()

        if(respuesta ==calcularRespuesta(tipoOperador, primerDigito, segundoDigito)){
            generarPuntaje(tiempo)
            Toast.makeText(activity, "Respuesta Correcta", Toast.LENGTH_LONG).show()
        }
        else{
            Toast.makeText(activity, "Respuesta Incorrecta", Toast.LENGTH_LONG).show()
        }
        numeroRondas++
        txt_round.text = getString(R.string.round, numeroRondas)
        resetGame()
    }

    fun resetGame(){
        countDownTimer.cancel()
        generarRandomicos()
        Log.i("mensaje", numeroRondas.toString())

        if(numeroRondas < 5){

            iniciarCronometro()
            countDownTimer.start()
        }
        else if(numeroRondas == 5){
            inputRespuesta.isVisible = false
            Toast.makeText(activity, "TU puntaje total es $score", Toast.LENGTH_LONG).show()
        }
    }

    fun generarRandomicos(){
        primerDigito = nextInt(1,20)
        segundoDigito = nextInt(1,primerDigito)
        txt_primer_digito.text = primerDigito.toString()
        txt_segundo_digito.text = segundoDigito.toString()

    }

    fun comprobarOperacion(tipoOperacion : String){
        if(tipoOperacion.equals("suma")){
            operador.text = "+"
        }else if(tipoOperacion.equals("resta")){
            operador.text = "-"
        }else if(tipoOperacion.equals("multiplicacion")){
            operador.text = "X"
        }else if(tipoOperacion.equals("division")){
            operador.text = "/"
        }
    }

    fun calcularRespuesta(tipoOperacion: String, primerDigito: Int, segundoDigito: Int): Double{
        var respuesta:Double = 0.0
        if(tipoOperacion.equals("suma")){
            respuesta = primerDigito+segundoDigito.toDouble()
        }else if(tipoOperacion.equals("resta")){
            respuesta = primerDigito-segundoDigito.toDouble()
        }else if(tipoOperacion.equals("multiplicacion")){
            respuesta = primerDigito*segundoDigito.toDouble()
        }else if(tipoOperacion.equals("division")){
            respuesta = primerDigito/segundoDigito.toDouble()
        }
        return respuesta
    }

    fun iniciarCronometro(){
        countDownTimer = object : CountDownTimer(timeLeft * 1000L, countDownInterval) {
            override fun onFinish() {
                timeLeft = 10
                txt_time_left.text = getString(R.string.time_left, timeLeft)
                //Toast.makeText(activity, "Tu puntaje es: $score", Toast.LENGTH_LONG)
                numeroRondas++
                txt_round.text = getString(R.string.round, numeroRondas)

                resetGame()
            }

            override fun onTick(millisUntilFinished: Long) {
                tiempo = millisUntilFinished.toInt() / 1000
                txt_time_left.text = getString(R.string.time_left, tiempo)
            }

        }

    }


    fun generarPuntaje(tiempo:Int){
        if(tiempo>8 && tiempo <=10){
            score += 100
        }else if(tiempo>5 && tiempo <=8){
            score += 50
        }else if(tiempo>0 && tiempo <=5){
            score += 10
        }
        txt_score.text = score.toString()

    }

    // TODO: Rename method, update argument and hook method into UI event
    fun onButtonPressed(uri: Uri) {
        listener?.onFragmentInteraction(uri)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnFragmentInteractionListener) {
            listener = context
        } else {
            throw RuntimeException(context.toString() + " must implement OnFragmentInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     *
     *
     * See the Android Training lesson [Communicating with Other Fragments]
     * (http://developer.android.com/training/basics/fragments/communicating.html)
     * for more information.
     */
    interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        fun onFragmentInteraction(uri: Uri)
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment Addition.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            Addition().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }
}
