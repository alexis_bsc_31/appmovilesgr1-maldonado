package com.example.alexis.examenib

import android.content.Context
import android.net.Uri
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.core.os.bundleOf
import androidx.navigation.Navigation.findNavController
import androidx.navigation.fragment.findNavController



// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Activities that contain this fragment must implement the
 * [Inicio.OnFragmentInteractionListener] interface
 * to handle interaction events.
 * Use the [Inicio.newInstance] factory method to
 * create an instance of this fragment.
 *
 */
class Inicio : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view:View = inflater.inflate(R.layout.fragment_inicio, container, false)
        var botonSuma = view.findViewById(R.id.suma) as Button
        var botonResta = view.findViewById(R.id.resta) as Button
        var botonMultiplicacion = view.findViewById(R.id.multiplicacion) as Button
        var botonDivision = view.findViewById(R.id.division) as Button

        botonSuma.setOnClickListener { clicSuma() }

        botonResta.setOnClickListener { clicResta() }

        botonMultiplicacion.setOnClickListener { clicMultiplicacion() }

        botonDivision.setOnClickListener { clicDivision() }
        return view
    }

    fun clicSuma(){
        val b = bundleOf("tipoOperador" to "suma")
        findNavController().navigate(R.id.action_addition, b)
    }

    fun clicResta(){
        val b = bundleOf("tipoOperador" to "resta")
        findNavController().navigate(R.id.action_addition, b)
    }

    fun clicMultiplicacion(){
        val b = bundleOf("tipoOperador" to "multiplicacion")
        findNavController().navigate(R.id.action_addition, b)
    }

    fun clicDivision(){
        val b = bundleOf("tipoOperador" to "division")
        findNavController().navigate(R.id.action_addition, b)
    }

}
