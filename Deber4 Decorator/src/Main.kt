interface ModeloAuto {
    fun getModelo()
}

class ModeloAutoBasico : ModeloAuto{
    override fun getModelo() {
        println("Es un Chevrolet Aveo")
    }
}
open class DecoradorModeloAuto(protected var modeloAuto: ModeloAuto) : ModeloAuto{
    override fun getModelo() {
        this.modeloAuto.getModelo()
    }
}

class SeguroModeloAuto(m:ModeloAuto) : DecoradorModeloAuto(m){

    override fun getModelo(){
        super.getModelo ()
        this.agregarSeguro()
        println("El auto esta asegurado")
    }
    fun agregarSeguro(){
        println("Agregando seguro al auto")
    }
}

class FullModeloAuto(m:ModeloAuto) : DecoradorModeloAuto(m){

    override fun getModelo(){
        super.getModelo ()
        this.agregarFullEquipo()
        println("El auto es full equipo")
    }
    fun agregarFullEquipo(){
        println("Se agrego un full equipo al auto")
    }
}

fun main() {
    val peanutMilkShake = FullModeloAuto(ModeloAutoBasico())
    peanutMilkShake.getModelo()
    val bananaMilkShake = SeguroModeloAuto(ModeloAutoBasico())
    bananaMilkShake.getModelo()
}