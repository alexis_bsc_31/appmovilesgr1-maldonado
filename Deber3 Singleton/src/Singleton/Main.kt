package Singleton

class Main {
    fun main(args: Array<String>) {

        val ricardo = Singleton.getSingletonInstance("Ricardo Moya")

        try {
            val richard = ricardo.clone()
            System.out.println(richard.getNombre())
        } catch (ex: NullPointerException) {
            ex.printStackTrace()
        }

    }
}