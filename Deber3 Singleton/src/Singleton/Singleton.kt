package Singleton

class Singleton(val name:String) {
    private var nombre: String = ""
    private var singleton: Singleton? = null

    // El constructor es privado, no permite que se genere un constructor por defecto.
    init{
        this.nombre = name
        println("Mi nombre es: " + this.nombre)
    }

    fun getSingletonInstance(nombre: String): Singleton{
        if (singleton == null) {
            singleton = Singleton(nombre)
        } else {
            println("No se puede crear el objeto $nombre porque ya existe un objeto de la clase SoyUnico")
        }

        return singleton as Singleton
    }

    fun getNombre():String{
        return this.nombre
    }

    fun clone(): Singleton? {
        try {
            throw CloneNotSupportedException()
        } catch (ex: CloneNotSupportedException) {
            println("No se puede clonar un objeto de la clase SoyUnico")
        }

        return null
    }

}