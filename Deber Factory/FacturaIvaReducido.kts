public class FacturaIvaReducido extends Factura{
    override fun getImporteIva(){
        return getImporte()*1.05;
    }
}