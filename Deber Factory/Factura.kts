public abstract class Factura{
    private var factura_id : Int;
    private var importe: Double;

    public fun getId: Int(){
        return this.factura_id;
    }

    public fun setId(factura_id: Int){
        this.factura_id = factura_id;
    }

    public fun getImporte: Double(){
        return this.importe;
    }

    public fun setImporte(importe:Double){
        this.importe = importe;
    }

    public abstract getImporte:Double();

}