class Ent_Usuario{
  String nombre;
  String cedula;
  String telefono;
  String email;
  String direccion_domicilio;

  void setUsuario(String nombre, String cedula, String telefono, String email, String direccion_domicilio){
    this.nombre = nombre;
    this.cedula = cedula;
    this.telefono = telefono;
    this.email = email;
    this.direccion_domicilio = direccion_domicilio;
  }

  String getNombreUsuario(){
    return this.nombre;
  }
}