import 'package:flutter/material.dart';
import 'package:proyectomobiles/generales.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class ImageFood extends StatefulWidget{
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _ImageFood();
  }
}

class _ImageFood extends State<ImageFood>{
  Color color_button = Colors.white;
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Container(
      width: 150,
      height: 150,
      color: Colors.black12,
      child: Stack(
        children: <Widget>[
          Text("Comida"),
          Align(
            alignment: Alignment(0.8, 0.8),
            child: Container(
              height: 40,
              width: 40,
              child: FloatingActionButton(
                onPressed: (){
                  setState(() {
                    color_button = Generales().primary;
                  });
                },
                backgroundColor: Generales().second,
                child: Icon(FontAwesomeIcons.plus, color: color_button, size: 25,),
              ),
            ),
          )
        ],
      )
    );
  }

}