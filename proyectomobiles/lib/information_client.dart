import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class InformationClient extends StatefulWidget{
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _InformationClient();
  }

}

class _InformationClient extends State<InformationClient>{
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Container(
      decoration: BoxDecoration(
        image: DecorationImage(
          fit: BoxFit.cover,
            image: AssetImage("assets/image/background_information_client.jpg"))

      ),
      child: Align(
            alignment: Alignment(1,-0.5),
            child: Container(
              margin: EdgeInsets.only(
                  top: 60.0,
                right: 40.0
              ),
              padding: EdgeInsets.only(
                top: 50.0,
                left: 50.0
              ),
              height: 500.0,
              width: 500.0,
              decoration: BoxDecoration(
                  image: DecorationImage(
                    fit: BoxFit.fitHeight,
                      image: AssetImage("assets/image/notebook_information.png"))
              ),
              child: Column(
                children: <Widget>[
                  Text(
                      'Información General',
                    style: TextStyle(
                      fontFamily: 'Gabriela',
                      fontSize: 25.0,
                      fontWeight: FontWeight.w900
                    ),
                    textAlign: TextAlign.center,
                  ),
                  Text(
                        '\nNombre: Alexis Maldonado'
                        '\n\nCédula: 1725790032'
                        '\n\nTeléfono: 0960267341'
                        '\n\nDirección: México y Río de Janeiro',
                    style: TextStyle(
                      fontFamily: 'Gabriela',
                      fontSize: 20.0
                    ),
                  ),
                  Stack(
                    children: <Widget>[
                      Container(
                        height: 100,
                        width: 300,

                        child: Align(
                          alignment: Alignment(0.7,0),
                          child: Transform.rotate(angle: 0.45,
                            child: IconButton(
                              icon: Icon(FontAwesomeIcons.smileBeam, size: 60.0,),
                              onPressed: (){
                                print('Hola');
                              },
                            ),
                          )
                        ),
                      ),

                    ],
                  )
                ],
              ),
            ),

          ),


    );
  }

}