import 'dart:async';

import 'package:flutter/material.dart';
import 'generales.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class Pedidos extends StatefulWidget{
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _Pedidos();
  }

}

class _Pedidos extends State<Pedidos>{

  var data;
  String nombre = "";
  String welcome_message = "Bienvenido ";
  Future<String> getData() async{
    var respuesta = await Firestore.instance.collection('datos_usuario').where('nombre', isEqualTo: 'Alexis').getDocuments().then((QuerySnapshot docs) {
      if(docs.documents.isNotEmpty){
        data = docs.documents[0].data;
      }
    });
    print('aqui');
    setState(() {
      welcome_message = 'Bienvenido ${data['nombre']}';
    });
    return respuesta;
  }

  /*@override
  void initState(){
    super.initState();
    print(getData());
    String name = data['nombre'];
    setState(() {
      welcome_message = 'Bienvenido $name';
    });

  }*/
  contenidoBoton(Icon icon, String text){
    return Column(
      children: <Widget>[
        Container(
            margin: EdgeInsets.only(
                top: 30.0
            ),
            child: icon
        ),
        Container(
          margin: EdgeInsets.only(
              top: 10.0
          ),
          padding: EdgeInsets.only(
              left: 5.0,
              right: 5.0
          ),
          child: Text(
            text,
            style: TextStyle(
                fontFamily: "Gabriela",
                fontSize: 15.0,
                fontWeight: FontWeight.w600
            ),
            textAlign: TextAlign.center,
          ),
        )
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Container(
      decoration: BoxDecoration(
          image: DecorationImage(
            fit: BoxFit.cover,
            image: AssetImage("assets/image/background_app.jpg"),
          )
      ),
      child: Column(

        children: <Widget>[
          Container(
            margin: EdgeInsets.only(
                top: 50.0
            ),
            child: Text(
              welcome_message,
              style: TextStyle(
                  fontFamily: "Gabriela",
                  fontSize: 30.0,
                  fontWeight: FontWeight.w900
              ),
              textAlign: TextAlign.center,
            ),
          ),
          Container(
            margin: EdgeInsets.only(
                top: 20.0,
                bottom: 20.0
            ),
            child: Text(
              '¿Qué deseas hacer hoy?',
              style: TextStyle(
                  fontFamily: "Gabriela",
                  fontSize: 20.0,
                  fontWeight: FontWeight.w300
              ),
              textAlign: TextAlign.center,
            ),
          ),
          Row(
            children: <Widget>[
              Container(
                  margin: EdgeInsets.only(
                      top: 50.0,
                      left: 30.0
                  ),
                  width: 160,
                  height: 160,
                  decoration: BoxDecoration(
                      gradient: LinearGradient(colors: [
                        Generales().primary_transparent,
                        Generales().second_transparent
                      ],
                          begin: FractionalOffset(0.2, 0.0),
                          end: FractionalOffset(1.0, 0.6),
                          stops: [0.0, 0.6],
                          tileMode: TileMode.clamp
                      ),
                      borderRadius: BorderRadius.all(Radius.circular(10.0)),
                      shape: BoxShape.rectangle,
                      boxShadow: <BoxShadow>[
                        BoxShadow(
                            color: Colors.black38,
                            blurRadius: 15.0,
                            offset: Offset(0.0, 7.0)
                        )
                      ]
                  ),
                  child: contenidoBoton(Icon(FontAwesomeIcons.fileInvoiceDollar, size: 60.0,), "Realizar un pedido")
              ),
              Container(
                margin: EdgeInsets.only(
                    top: 50.0,
                    left: 30.0
                ),
                width: 160,
                height: 160,
                decoration: BoxDecoration(
                    gradient: LinearGradient(colors: [
                      Generales().primary_transparent,
                      Generales().second_transparent
                    ],
                        begin: FractionalOffset(0.2, 0.0),
                        end: FractionalOffset(1.0, 0.6),
                        stops: [0.0, 0.6],
                        tileMode: TileMode.clamp
                    ),
                    borderRadius: BorderRadius.all(Radius.circular(10.0)),
                    shape: BoxShape.rectangle,
                    boxShadow: <BoxShadow>[
                      BoxShadow(
                          color: Colors.black38,
                          blurRadius: 15.0,
                          offset: Offset(0.0, 7.0)
                      )
                    ]
                ),
                child: contenidoBoton(Icon(FontAwesomeIcons.eye, size: 60.0,), "Ver mis pedidos"),

              )
            ],
          ),
          Row(
            children: <Widget>[
              Container(
                  margin: EdgeInsets.only(
                      top: 50.0,
                      left: 30.0
                  ),
                  width: 160,
                  height: 160,
                  decoration: BoxDecoration(
                      gradient: LinearGradient(colors: [
                        Generales().primary_transparent,
                        Generales().second_transparent
                      ],
                          begin: FractionalOffset(0.2, 0.0),
                          end: FractionalOffset(1.0, 0.6),
                          stops: [0.0, 0.6],
                          tileMode: TileMode.clamp
                      ),
                      borderRadius: BorderRadius.all(Radius.circular(10.0)),
                      shape: BoxShape.rectangle,
                      boxShadow: <BoxShadow>[
                        BoxShadow(
                            color: Colors.black38,
                            blurRadius: 15.0,
                            offset: Offset(0.0, 7.0)
                        )
                      ]
                  ),
                  child: contenidoBoton(Icon(FontAwesomeIcons.idCard, size: 60.0,), "Mirar mi Información General")
              ),
              Container(
                  margin: EdgeInsets.only(
                      top: 50.0,
                      left: 30.0
                  ),
                  width: 160,
                  height: 160,
                  decoration: BoxDecoration(
                      gradient: LinearGradient(colors: [
                        Generales().primary_transparent,
                        Generales().second_transparent
                      ],
                          begin: FractionalOffset(0.2, 0.0),
                          end: FractionalOffset(1.0, 0.6),
                          stops: [0.0, 0.6],
                          tileMode: TileMode.clamp
                      ),
                      borderRadius: BorderRadius.all(Radius.circular(10.0)),
                      shape: BoxShape.rectangle,
                      boxShadow: <BoxShadow>[
                        BoxShadow(
                            color: Colors.black38,
                            blurRadius: 15.0,
                            offset: Offset(0.0, 7.0)
                        )
                      ]
                  ),
                  child: contenidoBoton(Icon(FontAwesomeIcons.comments, size: 60.0,), "Dejar un Comentario")
              )
            ],
          ),
              IconButton(
              icon: Icon(FontAwesomeIcons.smileBeam, size: 60.0,),
    onPressed: getData
    )
        ],
      ),
    );
  }

}