import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:proyectomobiles/Entidades/Ent_Usuario.dart';

Future<Ent_Usuario> consultarInformacionGeneralCliente() async{
  Ent_Usuario ent_usuario = new Ent_Usuario();
  var data;    
  await Firestore.instance.collection('datos_usuario').where('nombre', isEqualTo: 'Alexis').getDocuments().then((QuerySnapshot docs) {
      if(docs.documents.isNotEmpty){
      data = docs.documents[0].data;
      }
  });
  
  ent_usuario.setUsuario(data['nombre'], data['cedula'], data['telefono'], '', data['direccion']);

  return ent_usuario;
}

