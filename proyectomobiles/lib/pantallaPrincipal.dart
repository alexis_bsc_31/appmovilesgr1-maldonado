import 'package:flutter/material.dart';
import 'pedidos.dart';
import 'cupones.dart';
import 'promociones.dart';
import 'information_client.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'generales.dart';
class PantallaPrincipal extends StatefulWidget{
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _PantallaPrincipal();
  }

}

class _PantallaPrincipal extends State<PantallaPrincipal> with SingleTickerProviderStateMixin{
  int tapIndex = 0;
  final List<Widget> widgetsChildren = [ Pedidos(), InformationClient(), Cupones()];
  TabController controller;
  @override
  void initState(){
    super.initState();
    controller = new TabController(length: 3, vsync: this);
  }
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      body: Container(
        child: TabBarView(
          children: widgetsChildren,
          controller: controller,

        ),
      ),
      bottomNavigationBar: Material(
        child: TabBar(
            tabs: <Tab>[
              Tab(
                child: Icon(FontAwesomeIcons.cashRegister, color: Generales().second),
              ),
              Tab(
                child: Icon(FontAwesomeIcons.tags, color: Generales().second,),
              ),
              Tab(
                child: Icon(FontAwesomeIcons.wallet, color: Generales().second),
              )
            ],
          indicatorColor: Generales().primary,
          controller: controller,

        ),
      ),
    );
  }

}