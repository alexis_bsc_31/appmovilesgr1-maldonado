import 'dart:ui';

class Generales{
  Color primary = Color.fromRGBO(254, 60, 50, 1);
  Color primary_transparent = Color.fromRGBO(254, 60, 50, 0.6);

  Color second = Color.fromRGBO(255, 147, 91, 1);
  Color second_transparent = Color.fromRGBO(255, 147, 91, 0.6);

}