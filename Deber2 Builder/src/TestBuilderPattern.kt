object TestBuilderPattern {

    @JvmStatic
    fun main(args: Array<String>) {
        val comp = Computador.ComputerBuilder(
            "500 GB", "2 GB"
        ).setBluetooth(true)
            .setTarjetaGrafica(true).build()

        print("Esta es su computadora\n")
        print(comp)
    }

}