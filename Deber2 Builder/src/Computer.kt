class Computador private constructor(builder: ComputerBuilder) {

    //required parameters
    private val hdd: String
    private val ram: String

    //optional parameters
    val isTarjetaGrafica: Boolean
    val isBluetooth: Boolean

    init {
        this.hdd = builder.HDD
        this.ram = builder.RAM
        this.isTarjetaGrafica= builder.isTarjetaGraficaBuilder
        this.isBluetooth= builder.isBluetoothBuilder
    }

    //Builder Class
    class ComputerBuilder(// required parameters
        val HDD: String, val RAM: String
    ) {

        // optional parameters
        var isTarjetaGraficaBuilder: Boolean = false
        var isBluetoothBuilder: Boolean = false

        fun setTarjetaGrafica(isGraphicsCardEnabled: Boolean): ComputerBuilder {
            this.isTarjetaGraficaBuilder= isGraphicsCardEnabled
            return this
        }

        fun setBluetooth(isBluetoothEnabled: Boolean): ComputerBuilder {
            this.isBluetoothBuilder= isBluetoothEnabled
            return this
        }

        fun build(): Computador {
            return Computador(this)
        }

    }

}