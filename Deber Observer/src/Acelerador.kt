import IObservador
class Acelerador : ISujetoObservable {

    lateinit var observador : IObservador

    constructor(

    )

    fun pisarAcelerador(){
        notificar()
    }
    fun enlazarObservador(o: IObservador){
        observador = o
    }
    override fun notificar() {
        observador.update()
    }

}