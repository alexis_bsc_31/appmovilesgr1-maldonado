import IObservador

class Motor : IObservador {

    constructor()

    override fun update() {
        print("Subir la velocidad")
    }

}