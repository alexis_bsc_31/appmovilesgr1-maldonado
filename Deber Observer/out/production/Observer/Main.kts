fun main(){
    var acelerador : Acelerador = Acelerador()

    var motor : Motor = Motor()

    acelerador.enlazarObservador(motor)

    acelerador.pisarAcelerador()
}

main()