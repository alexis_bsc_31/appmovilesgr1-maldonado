package com.example.time_fighter

import android.view.View
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView

class ScoresViewHolder(itemView: View):
    RecyclerView.ViewHolder(itemView) {
    val playerName = itemView.findViewById<TextView>(R.id.player_name_scores)
    val playerScore = itemView.findViewById<TextView>(R.id.player_score_scores)
}