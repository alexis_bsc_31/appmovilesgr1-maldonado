package com.example.time_fighter


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

/**
 * A simple [Fragment] subclass.
 *
 */
class ScoresFragment : Fragment() {

    lateinit var scoresRecyclerView: RecyclerView

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_scores, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        scoresRecyclerView = view.findViewById(R.id.scores_recycler_view)
        scoresRecyclerView.layoutManager = LinearLayoutManager(this.context)
        scoresRecyclerView.adapter = ScoresRecyclerViewAdapter()
    }


}
