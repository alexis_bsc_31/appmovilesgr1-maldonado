package com.example.time_fighter

import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.google.firebase.firestore.DocumentChange
import com.google.firebase.firestore.FirebaseFirestore

class ScoresRecyclerViewAdapter: RecyclerView.Adapter<ScoresViewHolder>() {

    val db = FirebaseFirestore.getInstance()
    var players: MutableList<Player> = mutableListOf<Player>()

    init {
        val playersRef = db.collection("players")

        playersRef
            .orderBy("score")
            .addSnapshotListener { snapshot, error ->
            if (error != null) {
                // show alert
                return@addSnapshotListener
            }

            for (doc in snapshot!!.documentChanges) {

                when (doc.type) {

                    DocumentChange.Type.ADDED -> {
                        val player = Player(
                            doc.document.getString("name")!!,
                            doc.document.getDouble("score")!!.toInt()
                        )

                        players.add(player)
                        notifyItemInserted(players.size - 1)
                    }
                    else -> return@addSnapshotListener
                }
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ScoresViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.scores_view_holder, parent, false)
        return ScoresViewHolder(view)
    }

    override fun getItemCount(): Int {
        return players.size
    }

    override fun onBindViewHolder(holder: ScoresViewHolder, position: Int) {
        holder.playerName.text = players[position].name
        holder.playerScore.text = players[position].score.toString()
    }

}